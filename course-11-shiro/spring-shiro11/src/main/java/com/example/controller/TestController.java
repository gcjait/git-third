package com.example.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
@Slf4j
@RestController
public class TestController {

    @GetMapping("/1")
    public Object test1() {
        return "ok1";
    }

    @RequiresAuthentication//需要登录认证，不能rememberme
    @GetMapping("/2")
    public Object test2() {
        return "ok2";
    }

    @RequiresAuthentication
    @RequiresPermissions("admin:test3")
    @GetMapping("/3")
    public Object test3() {
        return "ok3";
    }

    @RequiresRoles("admin")
    @GetMapping("/4")
    public Object test4() {
        return "ok4";
    }

    @RequiresRoles("admin")
    @GetMapping("/5")
    public Object test5() {
        return "ok5";
    }

    @RequiresRoles("vip")
    @GetMapping("/6")
    public Object test6() {
        return "ok6";
    }

}
