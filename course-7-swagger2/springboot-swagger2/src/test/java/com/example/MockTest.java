package com.example;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MockTest {

    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext context;

    /**
     * 准备测试环境
     */
    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void testGetUsers() throws Exception {

        //构造一个请求
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/user");

        // 执行一个请求
        ResultActions resultActions = mockMvc.perform(requestBuilder);

        //添加断言验证
        MvcResult mvcResult = resultActions
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("-1"))
                .andReturn();

        String res = mvcResult.getResponse().getContentAsString();
        System.out.println(res);
    }

    @Test
    public void testAddUsers() throws Exception {

        //构造一个请求
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/index");
//                .contentType(MediaType.APPLICATION_JSON)
//                .content("{ \"avatar\": \"string\", \"birthday\": \"2019-04-22T14:20:12.624Z\", \"commentCount\": 0, \"created\": \"2019-04-22T14:20:12.624Z\", \"email\": \"string\", \"gender\": \"string\", \"lasted\": \"2019-04-22T14:20:12.624Z\", \"mobile\": \"string\", \"modified\": \"2019-04-22T14:20:12.624Z\", \"password\": \"string\", \"point\": 0, \"postCount\": 0, \"sign\": \"string\", \"status\": 0, \"username\": \"string\", \"vipLevel\": \"string\", \"wechat\": \"string\"}");

        // 执行一个请求
        ResultActions resultActions = mockMvc.perform(requestBuilder);

        //添加断言验证
        MvcResult mvcResult = resultActions
                .andExpect(MockMvcResultMatchers.status().isOk())
//                .andExpect(MockMvcResultMatchers.view().name("index"))
                .andExpect(MockMvcResultMatchers.request().attribute("name", "吕一明33333"))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("0"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        String res = mvcResult.getResponse().getContentAsString();
        System.out.println(res);
    }

}
