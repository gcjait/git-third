package com.example;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@MapperScan("com.example.mapper")
@SpringBootApplication
public class SpringbootLearn2Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootLearn2Application.class, args);
        System.out.println("启动访问 ------------> http://localhost:8080");
    }

}
