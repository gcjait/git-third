package com.example.controller;


import com.example.config.Lv;
import com.example.entity.User;
import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.example.controller.BaseController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lv-success
 * @since 2019-04-08
 */
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

    @Autowired
    UserService userService;

    @GetMapping("/test")
    public User test() {
        return userService.getById(1L);
    }

}
