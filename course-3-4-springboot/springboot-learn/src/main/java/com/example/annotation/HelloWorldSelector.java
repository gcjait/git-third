package com.example.annotation;

import com.example.config.HelloWorldConfig;
import com.example.config.HelloWorldConfigSelector;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author 吕一明
 * @公众号 码客在线
 * @since ${date} ${time}
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import({HelloWorldConfigSelector.class})
public @interface HelloWorldSelector {

    boolean isLinux() default false;;
}
