package com.example.comsumer;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
@Component
@FeignClient(name = "PRODUCTER-CLIENT")
public interface ProducterClient {

}
