package com.example.config;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
public class IPasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence rawPassword) {
        return rawPassword.toString();
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return rawPassword.toString().equals(encodedPassword);
    }
}
