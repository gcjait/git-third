package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

@EnableScheduling
@Controller
@SpringBootApplication
public class SpringQuartzApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringQuartzApplication.class, args);
    }

    @GetMapping("/")
    public String index(HttpServletRequest request) {

        String token = "343943943943";//有意义的token
        request.setAttribute("token",  token);
        request.setAttribute("name", request.getServletContext().getAttribute("name"));
        return "index";
    }

    @PostMapping("/create")
    public String create(HttpServletRequest request) {

        String name = (String) request.getParameter("name");
        String token = (String) request.getParameter("token");

        //验证逻辑
        if(token != "343943943943") {
            return "index";
        }

        //过滤
        HTMLFilter filter = new HTMLFilter();
        name = filter.filter(name);


        System.out.println("验证name是错误的~~");

        request.getServletContext().setAttribute("name", name);

        request.setAttribute("name", name);
        return "index";
    }

}
