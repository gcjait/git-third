package com.example.simple;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
public class HelloScheduler {

    public static void main(String[] args) throws SchedulerException, InterruptedException {
        //定义一个jobDetail
        JobDetail jobDetail = JobBuilder.newJob(HelloJob.class).withIdentity("helloJob").build();

        //定义一个trigger
        CronTrigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("helloTrigger")
                .startNow()
                .withSchedule(
                        CronScheduleBuilder.cronSchedule("0/2 * * * * ?")
                )
                .build();


        //定义一个定时器容器
        StdSchedulerFactory factory = new StdSchedulerFactory();
        Scheduler scheduler = factory.getScheduler();
        scheduler.start();

        //容器运行job
        scheduler.scheduleJob(jobDetail, trigger);

        Thread.sleep(3000);

//        scheduler.standby();
        scheduler.pauseJob(new JobKey("helloJob"));

        Thread.sleep(3000);

        scheduler.resumeJob(new JobKey("helloJob"));

    }
}
